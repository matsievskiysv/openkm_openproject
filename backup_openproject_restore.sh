#!/usr/bin/env bash

if [ ! -f "$1" ]; then
    echo "Usage: $0 <backup>.tar.gz"
    exit 1
fi

if [ -d "openproject" ]; then
    echo "Cannot overwrite openproject directory. Delete first"
    exit 1
fi

tar -xvf "$1"
