Based on [openkm_deploy](https://gitlab.com/matsievskiysv/openkm_deploy) and [openproject](https://github.com/opf/openproject-deploy).

# Generate self-signed certificates

```bash
./nginx/gen_certs.sh
```

# Basic commands

## Bring up

```bash
docker-compose -f docker-compose_nginx.yml -f docker-compose_openkm.yml -f docker-compose_openproject.yml -f docker-compose_wikijs.yml up
```

## Bring down

```bash
docker-compose -f docker-compose_nginx.yml -f docker-compose_openkm.yml -f docker-compose_openproject.yml -f docker-compose_wikijs.yml down
```
