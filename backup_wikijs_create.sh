#!/usr/bin/env bash

if [ ! -d "$1" ] | [ ! -d "$2" ]; then
    echo "Usage: $0 <project_dir> <local_backup_dir> [<remote_backup_dir>]"
    exit 1
fi

pushd "$1"

set -a
. ./.env
set +a

docker-compose -f docker-compose_nginx.yml \
               -f docker-compose_openkm.yml \
               -f docker-compose_openproject.yml \
               -f docker-compose_wikijs.yml \
               stop

archive=${2}/wikijs_${WIKIJS_DOMAIN:-WikiJS}_$(date '+%Y-%m-%d').tar.gz

tar -cf ${archive} wikijs

docker-compose -f docker-compose_nginx.yml \
               -f docker-compose_openkm.yml \
               -f docker-compose_openproject.yml \
               -f docker-compose_wikijs.yml \
               start

# https://unix.stackexchange.com/a/129600/237921
if ! tar xOf ${archive} &> /dev/null; then
    ./send_email.py "${WIKIJS_DOMAIN:-WikiJS} backup problem" "There was an error during WikiJS backup for ${WIKIJS_DOMAIN:-WikiJS}"
fi

if [ -d "$3" ]; then
    rsync ${archive} "$3"
fi

popd
