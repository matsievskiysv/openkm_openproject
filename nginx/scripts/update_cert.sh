#!/usr/bin/env bash


find /etc/nginx/conf.d/ -type f -delete

acme_domains=""

for domain in $WELCOME_DOMAIN $OPENKM_DOMAIN $OPENPROJECT_DOMAIN $WIKIJS_DOMAIN
do
    cp /templates/encrypt.conf "/etc/nginx/conf.d/${domain}".conf
    sed -i \
        -e "s/_SERVERNAME_/$domain/g" \
        "/etc/nginx/conf.d/$domain".conf
    acme_domains="$acme_domains -d $domain"
done

service nginx force-reload

if [ "$1" = "update" ]; then
    ACTION="--renew-all"
else
    ACTION="--issue"
fi

if [ "${CERT_TESTING}" != "no" ]; then
    STAGING="--test"
else
    STAGING=""
fi

/root/.acme.sh/acme.sh \
    ${ACTION} ${STAGING} \
    $acme_domains \
    --nginx \
    --cert-file       /etc/ssl/certs/cert.pem  \
    --key-file       /etc/ssl/private/key.pem  \
    --reloadcmd     "service nginx force-reload"

find /etc/nginx/conf.d/ -type f -delete
/scripts/create_configs.sh

service nginx force-reload
