#!/usr/bin/env bash

if [[ ! -z $WELCOME_DOMAIN ]]
then
    cp /templates/serve.conf /etc/nginx/conf.d/welcome.conf
    sed -i \
	-e "s/_SERVERNAME_/$WELCOME_DOMAIN/g" \
	-e "s/_HTTP_PORT_/80/g" \
	-e "s/_HTTPS_PORT_/443/g" \
	-e "s/_HTTPS_URL_PORT_//g" \
	/etc/nginx/conf.d/welcome.conf

    if [[ ( ! -z $OPENKM_EXTRA_HTTP_PORT ) && ( ! -z $OPENKM_EXTRA_HTTPS_PORT ) ]]
    then
	if [[ -z $OPENKM_DOMAIN ]]
	then
	    cp /templates/proxy.conf /etc/nginx/conf.d/welcome_openkm.conf
	    sed -i \
		-e "s/_SERVERNAME_/$OPENKM_DOMAIN/g" \
		-e "s/_HTTP_PORT_/$OPENKM_EXTRA_HTTP_PORT/g" \
		-e "s/_HTTPS_PORT_/$OPENKM_EXTRA_HTTPS_PORT/g" \
		-e "s/_HTTPS_URL_PORT_/:$OPENKM_EXTRA_HTTPS_PORT/g" \
		-e "s|_REDIRECT_TO_|http://openkm:8080/|g" \
		/etc/nginx/conf.d/welcome_openkm.conf
	else
	    cp /templates/redirect.conf /etc/nginx/conf.d/welcome_openkm.conf
	    sed -i \
		-e "s/_SERVERNAME_/$WELCOME_DOMAIN/g" \
		-e "s/_REDIRECTNAME_/$OPENKM_DOMAIN/g" \
		-e "s/_HTTP_PORT_/$OPENKM_EXTRA_HTTP_PORT/g" \
		-e "s/_HTTPS_PORT_/$OPENKM_EXTRA_HTTPS_PORT/g" \
		-e "s/_HTTPS_URL_PORT_//g" \
		/etc/nginx/conf.d/welcome_openkm.conf
	fi
    fi

    if [[ ( ! -z $OPENPROJECT_EXTRA_HTTP_PORT ) && ( ! -z $OPENPROJECT_EXTRA_HTTPS_PORT ) ]]
    then
	if [[ -z $OPENPROJECT_DOMAIN ]]
	then
	    cp /templates/proxy.conf /etc/nginx/conf.d/welcome_openproject.conf
	    sed -i \
		-e "s/_SERVERNAME_/$OPENPROJECT_DOMAIN/g" \
		-e "s/_HTTP_PORT_/$OPENPROJECT_EXTRA_HTTP_PORT/g" \
		-e "s/_HTTPS_PORT_/$OPENPROJECT_EXTRA_HTTPS_PORT/g" \
		-e "s/_HTTPS_URL_PORT_/:$OPENPROJECT_EXTRA_HTTPS_PORT/g" \
		-e "s|_REDIRECT_TO_|http://openkm:8080/|g" \
		/etc/nginx/conf.d/welcome_openproject.conf
	else
	    cp /templates/redirect.conf /etc/nginx/conf.d/welcome_openproject.conf
	    sed -i \
		-e "s/_SERVERNAME_/$WELCOME_DOMAIN/g" \
		-e "s/_REDIRECTNAME_/$OPENPROJECT_DOMAIN/g" \
		-e "s/_HTTP_PORT_/$OPENPROJECT_EXTRA_HTTP_PORT/g" \
		-e "s/_HTTPS_PORT_/$OPENPROJECT_EXTRA_HTTPS_PORT/g" \
		-e "s/_HTTPS_URL_PORT_//g" \
		/etc/nginx/conf.d/welcome_openproject.conf
	fi
    fi

    if [[ ( ! -z $WIKIJS_EXTRA_HTTP_PORT ) && ( ! -z $WIKIJS_EXTRA_HTTPS_PORT ) ]]
    then
	if [[ -z $WIKIJS_DOMAIN ]]
	then
	    cp /templates/proxy.conf /etc/nginx/conf.d/wikijs.conf
	    sed -i \
		-e "s/_SERVERNAME_/$WELCOME_DOMAIN/g" \
		-e "s/_HTTP_PORT_/$WIKIJS_EXTRA_HTTP_PORT/g" \
		-e "s/_HTTPS_PORT_/$WIKIJS_EXTRA_HTTPS_PORT/g" \
		-e "s/_HTTPS_URL_PORT_/:$WIKIJS_EXTRA_HTTPS_PORT/g" \
		-e "s|_REDIRECT_TO_|http://wikijs:3000/|g" \
		/etc/nginx/conf.d/wikijs.conf
	else
	    cp /templates/redirect.conf /etc/nginx/conf.d/welcome_wikijs.conf
	    sed -i \
		-e "s/_SERVERNAME_/$WELCOME_DOMAIN/g" \
		-e "s/_REDIRECTNAME_/$WIKIJS_DOMAIN/g" \
		-e "s/_HTTP_PORT_/$WIKIJS_EXTRA_HTTP_PORT/g" \
		-e "s/_HTTPS_PORT_/$WIKIJS_EXTRA_HTTPS_PORT/g" \
		-e "s/_HTTPS_URL_PORT_//g" \
		/etc/nginx/conf.d/welcome_wikijs.conf
	fi
    fi
fi

if [[ ! -z $OPENKM_DOMAIN ]]
then
    cp /templates/proxy.conf /etc/nginx/conf.d/openkm.conf
    sed -i \
	-e "s/_SERVERNAME_/$OPENKM_DOMAIN/g" \
	-e "s/_HTTP_PORT_/80/g" \
	-e "s/_HTTPS_PORT_/443/g" \
	-e "s/_HTTPS_URL_PORT_//g" \
	-e "s|_REDIRECT_TO_|http://openkm:8080/|g" \
	/etc/nginx/conf.d/openkm.conf
fi

if [[ ! -z $OPENPROJECT_DOMAIN ]]
then
    cp /templates/proxy.conf /etc/nginx/conf.d/openproject.conf
    sed -i \
	-e "s/_SERVERNAME_/$OPENPROJECT_DOMAIN/g" \
	-e "s/_HTTP_PORT_/80/g" \
	-e "s/_HTTPS_PORT_/443/g" \
	-e "s/_HTTPS_URL_PORT_//g" \
	-e "s|_REDIRECT_TO_|http://web:8080/|g" \
	/etc/nginx/conf.d/openproject.conf
fi

if [[ ! -z $WIKIJS_DOMAIN ]]
then
    cp /templates/proxy.conf /etc/nginx/conf.d/wikijs.conf
    sed -i \
	-e "s/_SERVERNAME_/$WIKIJS_DOMAIN/g" \
	-e "s/_HTTP_PORT_/80/g" \
	-e "s/_HTTPS_PORT_/443/g" \
	-e "s/_HTTPS_URL_PORT_//g" \
	-e "s|_REDIRECT_TO_|http://wikijs:3000/|g" \
	/etc/nginx/conf.d/wikijs.conf
fi
