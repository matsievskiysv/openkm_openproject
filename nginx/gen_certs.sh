#!/usr/bin/env bash

set -e

DAYS=3650
SUBJ='/C=RU/ST=MSK/L=MSK/O=CMP/OU=CNO/CN=cert'

BASEDIR=$(dirname $(realpath $0))

if [[ -f $BASEDIR/.certs/cert.pem || -f $BASEDIR/.certs/key.pem ]]
then
    1>&2 echo Certificates exist! Not overwriting!
    exit 1
fi

openssl req -newkey rsa:4096 -x509 -sha256 -days $DAYS -nodes -subj $SUBJ \
	-out $BASEDIR/.certs/cert.pem \
	-keyout $BASEDIR/.certs/key.pem
