#!/usr/bin/env bash

if [ ! -f "$1" ]; then
    echo "Usage: $0 <backup>.tar.gz"
    exit 1
fi

if [ -d "openkm/files" ]; then
    echo "Cannot overwrite openkm/files directory. Delete first"
    exit 1
fi

tar -xvf "$1"
